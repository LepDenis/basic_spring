package com.formation.nike_lundi_7.services;

import java.util.Collection;
import java.util.Optional;

import com.formation.nike_lundi_7.models.Article;
import com.formation.nike_lundi_7.repositories.ArticleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticleService {

    private final ArticleRepository ar;

    @Autowired
    public ArticleService(ArticleRepository ar) {
        this.ar = ar;
    }

    public Collection<Article> findAll() {
        return ar.findAll();
    }

    public void delete(Long ref) {
        ar.deleteById(ref);
    }

    public Optional<Article> findById(Long ref) {
		return ar.findById(ref);
	}

	public void save(Article article) {
        ar.save(article);
	}
}
