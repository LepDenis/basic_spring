package com.formation.nike_lundi_7.services;

import java.util.Optional;

import com.formation.nike_lundi_7.models.User;
import com.formation.nike_lundi_7.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository ur;

    @Autowired
    public UserService(UserRepository ur) {
        this.ur = ur;
    }

    public void save(User user) {
        ur.save(user);
    }

    public Optional<User> findById(String email) {
        return ur.findById(email);
    }
}
