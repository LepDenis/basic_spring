package com.formation.nike_lundi_7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NikeLundi7Application {

	public static void main(String[] args) {
		SpringApplication.run(NikeLundi7Application.class, args);
	}

}
