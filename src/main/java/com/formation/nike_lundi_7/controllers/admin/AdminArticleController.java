package com.formation.nike_lundi_7.controllers.admin;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

import javax.validation.Valid;

import com.formation.nike_lundi_7.models.Article;
import com.formation.nike_lundi_7.services.ArticleService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin/articles")
public class AdminArticleController {

    private final ArticleService articleService;

    public AdminArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping("")
    public String index(Model m) {
        m.addAttribute("articles", articleService.findAll());
        m.addAttribute("fragment", "article/index");
        return "admin/index";
    }

    @GetMapping("{ref}")
    public String oneArticle(@PathVariable Long ref, @RequestParam String action, Model m) {

        Optional<Article> article = articleService.findById(ref);

        if (action.equals("delete")) {
            if (article.isPresent()) {
                File file = new File("src/main/resources/static/images/" + article.get().getImgLink());
                file.delete();
                articleService.delete(ref);
            }
            return "redirect:/admin/articles";
        }

        if (action.equals("update") && article.isPresent()) {
            m.addAttribute("article", article.get());
            m.addAttribute("action", "/" + ref + "?action=update");
            m.addAttribute("fragment", "article/form");
        }
        return "admin/index";
    }

    @PostMapping("{ref}")
    public String update(@Valid @ModelAttribute(name = "article") Article article, BindingResult articleBinding,
            @PathVariable Long ref, @RequestParam String action, @RequestParam(name = "image") MultipartFile image,
            Model m, RedirectAttributes attributes) {

        if (!articleBinding.hasErrors()) {

            boolean isValid = true;
            if (!image.isEmpty() && image.getContentType().equals("image/jpeg")
                    || image.getContentType().equals("image/webp") || image.getContentType().equals("image/heic")
                    || image.getContentType().equals("image/png")) {

                File img = new File("src/main/resources/static/images/" + image.getOriginalFilename());
                try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img))) {

                    bos.write(image.getBytes());
                    if (new File("src/main/resources/static/images/" + article.getImgLink()).delete()) {
                        article.setImgLink(image.getOriginalFilename());
                    }
                } catch (IOException e) {
                    isValid = false;
                    m.addAttribute("action", "/" + ref + "?action=update");
                    m.addAttribute("fragment", "article/form");
                    m.addAttribute("errormessage", "Un problème de sauvegarde est survenu !");
                }
            }
            if (isValid) {
                articleService.save(article);
                attributes.addFlashAttribute("message", "L'article " + ref + " a bien été mis à jour !");
                return "redirect:/admin/articles";
            }
        }
        m.addAttribute("action", "/" + ref + "?action=update");
        m.addAttribute("fragment", "article/form");
        return "/admin/index";
    }

    @GetMapping("add")
    public String getCreate(Model m, Article article) {
        m.addAttribute("action", "/add");
        m.addAttribute("fragment", "article/form");
        return "/admin/index";
    }

    @PostMapping("add")
    public String create(@Valid @ModelAttribute(name = "article") Article article, BindingResult articleBinding,
            @RequestParam(name = "image") MultipartFile image, Model m, RedirectAttributes attributes) {

        if (!articleBinding.hasErrors()) {

            boolean isValid = true;
            if (!image.isEmpty() && image.getContentType().equals("image/jpeg")
                    || image.getContentType().equals("image/webp") || image.getContentType().equals("image/heic")
                    || image.getContentType().equals("image/png")) {

                File img = new File("src/main/resources/static/images/" + image.getOriginalFilename());
                try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img))) {
                    bos.write(image.getBytes());
                    article.setImgLink(image.getOriginalFilename());
                } catch (IOException e) {
                    isValid = false;
                    m.addAttribute("action", "/add");
                    m.addAttribute("fragment", "article/form");
                    m.addAttribute("errormessage", "Un problème de sauvegarde est survenu !");
                }
            }
            if (isValid) {
                articleService.save(article);
                attributes.addFlashAttribute("message", "L'article " + article.getName() + " a bien été créé !");
                return "redirect:/admin/articles";
            }
        }
        m.addAttribute("action", "/add");
        m.addAttribute("fragment", "article/form");
        return "/admin/index";
    }

}
