package com.formation.nike_lundi_7.controllers;

import java.util.Base64;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.formation.nike_lundi_7.enums.RoleEnum;
import com.formation.nike_lundi_7.models.User;
import com.formation.nike_lundi_7.services.ArticleService;
import com.formation.nike_lundi_7.services.UserService;
import com.formation.nike_lundi_7.utils.PasswordEncoderJava;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexController {

    private ArticleService articleService;
    private UserService userService;

    @Autowired
    public IndexController(ArticleService articleService, UserService userService) {
        this.articleService = articleService;
        this.userService = userService;
    }

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("fragment", "video");
        return "index";
    }

    @GetMapping("/articles")
    public String articles(Model m) {
        m.addAttribute("articles", articleService.findAll());
        m.addAttribute("fragment", "articles");
        return "index";
    }

    @GetMapping("/inscription")
    public String getForm(Model m, User user) {
        m.addAttribute("fragment", "inscription");
        return "index";
    }

    @GetMapping("/login")
    public String getLogin(Model m, User user) {
        m.addAttribute("fragment", "login");
        return "index";
    }

    @PostMapping("/inscription")
    public String postForm(Model m, @Valid @ModelAttribute(name = "user") User user, BindingResult userBinding) {

        if (!userBinding.hasErrors()) {
            if (userService.findById(user.getEmail()).isEmpty()) {

                PasswordEncoderJava pwdEncoder = new PasswordEncoderJava();
                byte[] salt = pwdEncoder.generateSalt();
                user.setSalt(Base64.getEncoder().encodeToString(salt));

                byte[] password = pwdEncoder.generatePassword(user.getPassword(), salt);
                user.setPwdHash(Base64.getEncoder().encodeToString(password));

                user.setRole(RoleEnum.ADMIN);

                userService.save(user);
                return "redirect:/";

            } else
                m.addAttribute("emailExists", "Il existe déjà un compte avec cette adresse E-Mail !");
        }
        m.addAttribute("fragment", "inscription");
        return "index";
    }

    @PostMapping("/login")
    public String postLogin(Model m, @Valid @ModelAttribute(name = "user") User user, BindingResult userBinding,
            HttpSession session) {

        if (!userBinding.hasErrors()) {
            Optional<User> optUser = userService.findById(user.getEmail());

            if (optUser.isPresent()) {
                PasswordEncoderJava pwdEncoder = new PasswordEncoderJava();
                byte[] bytesTable = pwdEncoder.generatePassword(user.getPassword(),
                        Base64.getDecoder().decode(optUser.get().getSalt()));

                if ((Base64.getEncoder().encodeToString(bytesTable)).equals(optUser.get().getPwdHash())) {
                    session.setAttribute("user", user);
                    return "redirect:/";
                }
            }
            m.addAttribute("accountError", "E-mail ou Mot de Passe incorrect !");
        }
        m.addAttribute("fragment", "login");
        return "index";
    }

    @GetMapping("/logout")
    public String getLogout(HttpSession session) {
        // session.setAttribute("user", null);
        session.invalidate();
        return "redirect:/";
    }

}