package com.formation.nike_lundi_7.repositories;

import com.formation.nike_lundi_7.models.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    
}
