package com.formation.nike_lundi_7.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.formation.nike_lundi_7.enums.RoleEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @Email
    @NotBlank
    @NotNull
    private String email;

    @Column(length = 155, nullable = false)
    @Pattern(regexp = "^[a-z \\À-ÖØ-öø-ÿ]{2,155}$", flags = { Pattern.Flag.CASE_INSENSITIVE,
            Pattern.Flag.DOTALL }, message = "Ne peut contenir que des majuscules, minucules et espaces !")
    private String lastName;

    @Column(length = 155, nullable = false)
    @Pattern(regexp = "^[a-z \\À-ÖØ-öø-ÿ]{2,155}$", flags = { Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.DOTALL },
            message = "{perso.javax.constraints.name}")
    private String firstName;

    @Transient
    @Pattern(regexp = "^.{6,32}$", flags = { Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.DOTALL })
    private String password;

    @Column(length = 150, nullable = false)
    private String pwdHash;

    @Column(length = 150, nullable = false)
    private String salt;

    @Enumerated(EnumType.STRING)
    private RoleEnum role;
    
}
