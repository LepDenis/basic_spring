package com.formation.nike_lundi_7.models;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class Article {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long ref;

	@Column(nullable = false)
	@NonNull
	@Pattern(regexp = "^[a-z 0-9.\\-_\\\\/À-ÖØ-öø-ÿ]{3,255}$", flags = {Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.DOTALL})
	private String name;

	@Column(nullable = false)
	private String imgLink;

	@Column
	@NonNull
	private String shortDesc;
	
	@Column(columnDefinition = "TEXT")
	@NonNull
	private String longDesc;

	
}
